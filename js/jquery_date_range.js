/**
 * @file
 * Date range js integration.
 */

(function ($) {
    Drupal.behaviors.jqueryDateRange = {
        attach: function() {
            var $widget = $('div.form-type-jdate-picker').parents('fieldset.form-type-jdate-picker').once('date');
            var i;
            for (i = 0; i < $widget.length; i++) {
                new Drupal.jquery_date_range.DateHandler($widget[i]);
            }
        }
    }

    Drupal.jquery_date_range = Drupal.jquery_date_range || {};

    Drupal.jquery_date_range.DateHandler = function (widget) {
        this.$widget = $(widget);
        this.$start = this.$widget.find('.date-range-select-value');
        this.$end = this.$widget.find('.date-range-select-value2');
        var startid = this.$start.attr('id');
        var endid = this.$end.attr('id');
        var pickerid = this.$widget.attr('id');

        var formatid = pickerid.replace(/-/g,'_');

        $('#' + startid + ', #' + endid).dateRangePicker({
            separator : ' to ',
            autoClose: true,
            format: Drupal.settings['jdate_picker_' + formatid].date_format,
            getValue: function() {
                if ($('#'+startid).val() && $('#'+ endid).val() )
                    return $('#'+ startid).val() + ' to ' + $('#'+ endid).val();
                else
                    return '';
            },
            setValue: function(s,s1,s2) {
                $('#'+ startid).val(s1);
                $('#'+ endid).val(s2);
            }
        });
    };

})(jQuery);