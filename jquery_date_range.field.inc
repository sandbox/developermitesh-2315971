<?php
/**
 * @file
 * jquery_date_range field type
 */

/**
 * Implements hook_field_info().
 */
function jquery_date_range_field_info() {
  return array(
    'jdate_picker' => array(
      'label' => t('Jquery Date Range'),
      'description' => t('This field stores date range'),
      'settings' => array(
        'two_fields' => TRUE,
        'date_format' => 'DD-MM-YYYY',
      ),
      'instance_settings' => array(
        'date_format' => 'DD-MM-YYYY',
      ),
      'default_widget' => 'jdate_picker_default',
      'default_formatter' => 'jdate_picker_default',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function jquery_date_range_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];
  $form = array();

  if ($field['type'] == 'jdate_picker') {
    $options = array();

    $form['two_fields'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Start date and End Date two fields?'),
      '#default_value' => !empty($settings['two_fields']) ? $settings['two_fields'] : TRUE,
    );

    $form['two_fields'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Start date and End Date two fields?'),
      '#default_value' => !empty($settings['two_fields']) ? $settings['two_fields'] : TRUE,
    );
  }

  return $form;
}

/**
 * Implements hook_field_is_empty().
 */
function jquery_date_range_field_is_empty($item, $field) {
  if ($field['type'] == 'jdate_picker') {
    // profile_id = 0 is empty too, which is exactly what we want.
    return (empty($item['start_date']) || empty($item['end_date'])) ;
  }
}

/**
 * Implements hook_field_widget_info().
 *
 * Defines widgets available for use with field types as specified in each
 * widget's $info['field types'] array.
 */
function jquery_date_range_field_widget_info() {
  $widgets = array();

  // Define the creation / reference widget for line items.
  $widgets['jdate_picker_default'] = array(
    'label' => t('Jquery Date Picker'),
    'description' => t('Jquery Date Picker'),
    'field types' => array('jdate_picker'),
    'settings' => array(),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 *
 * Used to define the form element for custom widgets.
 */
function jquery_date_range_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Define the complex customer profile reference field widget.
  if ($instance['widget']['type'] == 'jdate_picker_default') {
    $element['#process'][] = 'jquery_date_range_jdate_picker_process';
    $element['#theme_wrappers'][] = 'jquery_date_range_jdate_picker';
    return $element;
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function jquery_date_range_field_formatter_info() {
  return array(
    'jdate_picker_default' => array(
      'label' => t('Jquery Date Range'),
      'description' => t('Date Range'),
      'field types' => array('jdate_picker'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function jquery_date_range_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $result = array();

  switch ($display['type']) {
    case 'jdate_picker_default':
      foreach ($items as $delta => $item) {
          $result[$delta] = array(
            '#markup' => drupal_render($item),
          );
      }
      break;
  }
  return $result;
}